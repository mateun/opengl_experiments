// opengl_experiments.cpp : Defines the entry point for the console application.
//

#include "stdafx.h"
#define SDL_MAIN_HANDLED
#include <stdio.h>
#include <SDL.h>
#include <GL/glew.h>
#include <sys/stat.h>
#include <stdlib.h>
#include <fstream>
#include <sstream>
#include <string>
#include <assimp/Importer.hpp>
#include <assimp/scene.h>
#include <assimp/postprocess.h>
#include <glm/mat4x4.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include <glm/vec3.hpp>
#include <glm/gtc/type_ptr.hpp>
#include <glm/gtc/matrix_access.hpp>

#include "imgui/imgui.h"
#include "imgui/imgui_impl_sdl.h"
#include "imgui/imgui_impl_opengl3.h"

#include <fmod.hpp>
#include <fmod_common.h>


#include "model.h"
#include "fbmath.h"
#include "texture.h"
#include "shaderprogram.h"
#include "camera.h"
#include "geometry.h"
#include "sprite.h"
#include "framebuffer.h"
#include "states.h"
#include "entity.h"
#include "macros.h"
#include "scene.h"
#include "skeletal_mesh.h"

using std::ifstream;
using std::ios;
using std::ostringstream;

// Globals (uuuuhhhh...)
GameState g_gameState;
DebugState g_debugState;
bool g_useDebugCam = false;
fb::Camera* g_DebugCamera;
fb::Camera* g_GameCamera;
fb::Camera* g_ActiveCamera;
const Uint8 *g_kbState;
bool g_pauseGame = false;
float g_camSpeed = 0.05f;
float g_camRotSpeed = 0.01f;
float g_effectiveMouseX = 0;
float g_effectiveMouseY = 0;
int g_ScreenWidth = 1600, g_ScreenHeight = 800;
fb::Scene* g_sceneLevel1;

fb::Entity* g_TankBody;
fb::Entity* g_TankTurret;
fb::Entity* gunEntity;
fb::CameraEntity* g_DebugCameraEntity;
fb::CameraEntity* g_GameCameraEntity;
fb::CameraEntity* g_ActiveCameraEntity;

fb::Model* tankBodyModel;
fb::Model* tankTurretModel;
fb::Model* coordinateSystemModel;
fb::Model* projectileModel;

fb::SkeletalMesh* cubicSM;

FMOD::System     *g_soundSystem;
FMOD::Sound      *sound1, *sound2, *sound3;
FMOD::Channel    *channel = 0;

void setupSkeletalMeshes() {
	cubicSM = new fb::SkeletalMesh();
	cubicSM->initializeFromFile("D:/Projects/C++/opengl_experiments/opengl_experiments/models/plane_sm.dae");

}

void initFmodSound() {
	
	FMOD_RESULT       result;
	unsigned int      version;
	void             *extradriverdata = 0;

	result = FMOD::System_Create(&g_soundSystem);
	result = g_soundSystem->getVersion(&version);
	
	if (version < FMOD_VERSION) {
		SDL_Log("wrong fmod version, exiting!");
		exit(-1);
	}

	result = g_soundSystem->init(32, FMOD_INIT_NORMAL, extradriverdata);

	result = g_soundSystem->createSound("D:/Projects/C++/opengl_experiments/opengl_experiments/sounds/small_laser.wav", FMOD_DEFAULT | FMOD_LOOP_OFF, 0, &sound1);
	if (result != FMOD_OK) {
		SDL_Log("error loading sound!");
		exit(-1);
	}


}



// Imgui variables
ImGuiTextBuffer logBuf;
bool showDebugWindow = false;
bool scrollLogToBottom;

fb::Model* importModelFromFile(const char* fileName, glm::vec2 textureScale = glm::vec2(1)) {
	Assimp::Importer importer;
	const aiScene* scene = importer.ReadFile(fileName, aiProcess_Triangulate /*| aiProcess_FlipWindingOrder | aiProcess_GenUVCoords | aiProcess_FixInfacingNormals | aiProcess_MakeLeftHanded*/);

	if (!scene) {
		SDL_Log("can not find model file!");
		exit(-1);
	}

	aiMesh* mesh = scene->mMeshes[0];

	float* vertices = new float[mesh->mNumVertices * 3];
	float* normals = new float[mesh->mNumVertices * 3];
	float* uvs = new float[mesh->mNumVertices * 2];
	uint32_t* indeces = new uint32_t[mesh->mNumFaces * 3];
	int p = 0;
	int u = 0;
	int i = 0;
	for (int m = 0; m < mesh->mNumVertices; m++) {
		aiVector3D v = mesh->mVertices[m];
		vertices[p] = v.x;
		vertices[p + 1] = v.y;
		vertices[p + 2] = v.z;


		aiVector3D n = mesh->mNormals[m];
		normals[p] = n.x;
		normals[p + 1] = n.y;
		normals[p + 2] = n.z;

		p += 3;


		if (mesh->HasTextureCoords(0)) {
			aiVector3D t = mesh->mTextureCoords[0][m];
			uvs[u] = t.x * textureScale.x;
			uvs[u + 1] = (1 - t.y) * textureScale.y;	// otherwise the texture is wrong direction coming from blender
			u += 2;
		}
	}

	for (unsigned int f = 0; f < mesh->mNumFaces; f++) {
		aiFace face = mesh->mFaces[f];
		int findices = face.mNumIndices;
		indeces[i] = face.mIndices[0];
		indeces[i + 1] = face.mIndices[1];
		indeces[i + 2] = face.mIndices[2];
		i += 3;
	}

	fb::Model* m = new fb::Model(vertices, mesh->mNumVertices, normals, mesh->mNumVertices, uvs, mesh->mNumVertices);
	return m;
}


GLuint importModelFromFile(const char* fileName, int& nrVerts) {
	Assimp::Importer importer;
	const aiScene* scene = importer.ReadFile(fileName, aiProcess_Triangulate /*| aiProcess_FlipWindingOrder | aiProcess_GenUVCoords | aiProcess_FixInfacingNormals | aiProcess_MakeLeftHanded*/);

	if (!scene) {
		SDL_Log("can not find model file!");
		exit(-1);
	}

	aiMesh* mesh = scene->mMeshes[0];

	nrVerts = mesh->mNumVertices;

	float* vertices = new float[mesh->mNumVertices * 3];
	float* normals = new float[mesh->mNumVertices * 3];
	float* uvs = new float[mesh->mNumVertices * 2];
	uint32_t* indeces = new uint32_t[mesh->mNumFaces * 3];
	int p = 0;
	int u = 0;
	int i = 0;
	for (int m = 0; m < mesh->mNumVertices; m++) {
		aiVector3D v = mesh->mVertices[m];
		vertices[p] = v.x;
		vertices[p + 1] = v.y;
		vertices[p + 2] = v.z;


		aiVector3D n = mesh->mNormals[m];
		normals[p] = n.x;
		normals[p + 1] = n.y;
		normals[p + 2] = n.z;

		p += 3;


		if (mesh->HasTextureCoords(0)) {
			aiVector3D t = mesh->mTextureCoords[0][m];
			uvs[u] = (t.x);
			uvs[u + 1] = (1 - t.y);		// otherwise the texture is wrong direction coming from blender
			u += 2;
		}
	}

	for (unsigned int f = 0; f < mesh->mNumFaces; f++) {
		aiFace face = mesh->mFaces[f];
		int findices = face.mNumIndices;
		indeces[i] = face.mIndices[0];
		indeces[i + 1] = face.mIndices[1];
		indeces[i + 2] = face.mIndices[2];
		i += 3;
	}

	GLuint vao;
	glGenVertexArrays(1, &vao);
	glBindVertexArray(vao);
	GLuint vbuf;
	glGenBuffers(1, &vbuf);
	glBindBuffer(GL_ARRAY_BUFFER, vbuf);
	glBufferData(GL_ARRAY_BUFFER, sizeof(float) * 3 * nrVerts, vertices, GL_STATIC_DRAW);
	glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 0, NULL);
	glEnableVertexAttribArray(0);

	return vao;

	//_vb = new VertexBuffer(renderer, vertices, mesh->mNumVertices, VertexBufferType::Position);
	//_vbuvs = new VertexBuffer(renderer, uvs, mesh->mNumVertices, VertexBufferType::UVs);
	//_normals = new VertexBuffer(renderer, normals, mesh->mNumVertices, VertexBufferType::Normal);
	//_ib = new IndexBuffer(renderer, indeces, mesh->mNumFaces * 3);




}

bool fileExists(const char* fileName)
{
	struct stat info;
	int ret = -1;

	ret = stat(fileName, &info);
	return 0 == ret;
}

const std::string readCodeFromFile(const char* file) {
	ifstream inFile(file, ios::in);
	if (!inFile) {
		return 0;
	}

	ostringstream code;
	while (inFile.good()) {
		int c = inFile.get();
		if (!inFile.eof()) code << (char)c;
	}
	inFile.close();

	return code.str();
}

GLuint createShaderProgram(const char* vsfile, const char* fsfile) {
	
	GLuint vshader = glCreateShader(GL_VERTEX_SHADER);
	const std::string vsstring = readCodeFromFile(vsfile);
	const GLchar* vssource_char = vsstring.c_str();
	SDL_Log("vs source: %s", vssource_char);
	glShaderSource(vshader, 1, &vssource_char, NULL);
	glCompileShader(vshader);
	GLint compileStatus;
	glGetShaderiv(vshader, GL_COMPILE_STATUS, &compileStatus);
	SDL_Log("vshader compile status: %d", compileStatus);
	if (GL_FALSE == compileStatus) {
		SDL_Log("problem with vshader!!");
		exit(1);
	}


	GLuint fshader = glCreateShader(GL_FRAGMENT_SHADER);
	const std::string fsstring = readCodeFromFile(fsfile);
	const GLchar* fssource_char = fsstring.c_str();
	glShaderSource(fshader, 1, &fssource_char, NULL);
	glCompileShader(fshader);
	glGetShaderiv(fshader, GL_COMPILE_STATUS, &compileStatus);
	SDL_Log("fshader compile status: %d", compileStatus);

	GLuint program = glCreateProgram();
	glAttachShader(program, vshader);
	glAttachShader(program, fshader);
	glLinkProgram(program);
	glGetProgramiv(program, GL_LINK_STATUS, &compileStatus);
	GLenum err = glGetError();
	if (err != 0) SDL_Log("error: %d", err);
	SDL_Log("linkstatus: %d", compileStatus);

	glDeleteShader(vshader);
	glDeleteShader(fshader);

	return program;
}

void vectorTests() {
	fb::Vec4 v1(1, 2, 3, 1);
	fb::Vec4 v2(2, 5, 7, 1);
	fb::Vec4 v3 = v1 + v2;
	assert(v3.x == 3);
	assert(v3.y == 7);
	assert(v3.z == 10);

	v3 = v2 - v1;
	assert(v3.x == 1);
	fb::Vec4 v3n = v3.normalize();
	float v1len = v1.length();

	float matvalues[16] = {
		1, 0, 0, 0, 
		0, 1, 0, 0, 
		0, 0, 1, 0, 
		0, 0, 0, 1
	};
	fb::Mat4 mat(matvalues);
	assert(mat.m11 == 1);
	assert(mat.m12 == 0);
	assert(mat.m21 == 0);
	assert(mat.m22 == 1);

	fb::Vec4 vresult = mat * v1;
	assert(vresult.x == v1.x);
	assert(vresult.y == v1.y);
	assert(vresult.z == v1.z);
	
}

void AddLog(const char* fmt, ...)
{
	va_list args;
	va_start(args, fmt);
	logBuf.appendfv(fmt, args);
	va_end(args);
	scrollLogToBottom = true;
}

void setupCameras() {
	g_DebugCamera = new fb::Camera(glm::vec3(0, 10, 40), glm::vec3(0, 0, -1), glm::vec3(1, 0, 0), glm::vec3(0, 1, 0));
	g_DebugCamera->initializePerspective(70);

	g_GameCamera = new fb::Camera(glm::vec3(0, 20, 10), glm::vec3(0, 0, -1), glm::vec3(1, 0, 0), glm::vec3(0, 1, 0));
	g_GameCamera->initializePerspective(70);

	g_DebugCameraEntity = new fb::CameraEntity(glm::vec3(0, 0, 0), MATID, "debugCam", g_DebugCamera, true);
	g_GameCameraEntity = new fb::CameraEntity(glm::vec3(0, 0.6f, 0.2f), MATID, "gameCam", g_GameCamera, false);
}

void debugCamMovement() {

	int mouseX = 0, mouseY = 0;
	

	if (SDL_GetRelativeMouseState(&mouseX, &mouseY) & SDL_BUTTON(SDL_BUTTON_RIGHT)) {
		g_effectiveMouseX = mouseX;
		g_effectiveMouseY = mouseY;
		g_DebugCamera->_forward = glm::rotate(glm::mat4(1.0f), -g_camRotSpeed * mouseX, g_DebugCamera->_up) *
			glm::vec4(g_DebugCamera->_forward.x, g_DebugCamera->_forward.y, g_DebugCamera->_forward.z, 1);
		//gf = glm::rotate(glm::mat4(1.0f), -g_camRotSpeed * mouseX, gu) * glm::vec4(gf, 1);

		g_DebugCamera->_right = glm::rotate(glm::mat4(1.0f), -g_camRotSpeed * mouseX, g_DebugCamera->_up) *
			glm::vec4(g_DebugCamera->_right.x, g_DebugCamera->_right.y, g_DebugCamera->_right.z, 1);
		g_DebugCamera->_right.y = 0;

		//gr = glm::rotate(glm::mat4(1), -g_camRotSpeed * mouseX, gu) * glm::vec4(gr, 1);


		g_DebugCamera->_forward = glm::rotate(glm::mat4(1.0f), -g_camRotSpeed * mouseY, g_DebugCamera->_right) *
			glm::vec4(g_DebugCamera->_forward.x, g_DebugCamera->_forward.y, g_DebugCamera->_forward.z, 1);
		//gf = glm::rotate(glm::mat4(1.0f), -g_camRotSpeed * mouseY, gr) * glm::vec4(gf, 1);
		//gu = glm::rotate(glm::mat4(1), -g_camRotSpeed * mouseY, gr) * glm::vec4(gu, 1);


	}

	if (g_kbState[SDL_SCANCODE_I]) {

		/*vec<3, T, Q> const f(normalize(center - eye));
		vec<3, T, Q> const s(normalize(cross(up, f)));
		vec<3, T, Q> const u(cross(f, s));*/

		// pitch = rotate about local "right" axis
		g_DebugCamera->_forward = glm::rotate(glm::mat4(1.0f), -g_camRotSpeed, g_DebugCamera->_right) * glm::vec4(g_DebugCamera->_forward.x, g_DebugCamera->_forward.y, g_DebugCamera->_forward.z, 1);
		//g_DebugCamera->_up = glm::rotate(glm::mat4(1.0f), -0.01f, g_DebugCamera->_right) * glm::vec4(g_DebugCamera->_up.x, g_DebugCamera->_up.y, g_DebugCamera->_up.z, 1);
	}

	if (g_kbState[SDL_SCANCODE_J]) {
		// pitch = rotate about local "right" axis
		g_DebugCamera->_forward = glm::rotate(glm::mat4(1.0f), g_camRotSpeed, g_DebugCamera->_right) * glm::vec4(g_DebugCamera->_forward.x, g_DebugCamera->_forward.y, g_DebugCamera->_forward.z, 1);
		//g_DebugCamera->_up = glm::rotate(glm::mat4(1.0f), 0.01f, g_DebugCamera->_right) * glm::vec4(g_DebugCamera->_up.x, g_DebugCamera->_up.y, g_DebugCamera->_up.z, 1);
	}

	if (g_kbState[SDL_SCANCODE_D]) {
		// strafe = move along the right axis
		g_DebugCamera->pos += g_DebugCamera->_right * g_camSpeed;
		//g_DebugCamera->_right = glm::rotate(glm::mat4(1.0f), -0.01f, g_DebugCamera->_up) * glm::vec4(g_DebugCamera->_right.x, g_DebugCamera->_right.y, g_DebugCamera->_right.z, 1);
	}

	if (g_kbState[SDL_SCANCODE_A]) {
		// strafe = move along the right axis
		g_DebugCamera->pos -= g_DebugCamera->_right * g_camSpeed;
		//g_DebugCamera->_forward = glm::rotate(glm::mat4(1.0f), 0.01f, g_DebugCamera->_up) * glm::vec4(g_DebugCamera->_forward.x, g_DebugCamera->_forward.y, g_DebugCamera->_forward.z, 1);
		//g_DebugCamera->_right = glm::rotate(glm::mat4(1.0f), 0.01f, g_DebugCamera->_up) * glm::vec4(g_DebugCamera->_right.x, g_DebugCamera->_right.y, g_DebugCamera->_right.z, 1);
	}


	if (g_kbState[SDL_SCANCODE_W]) {
		g_DebugCamera->pos += g_DebugCamera->_forward * g_camSpeed;
	}

	if (g_kbState[SDL_SCANCODE_S]) {
		g_DebugCamera->pos -= g_DebugCamera->_forward * g_camSpeed;
	}

	g_DebugCamera->updateFromDirections();
}

class BodyUpdater : public fb::UpdateScript {
	void run(float ft, fb::Entity* owner) override {
		owner->_relativePos += glm::vec3(0, 0, -0.000f);
	} 
};

class TurretUpdater : public fb::UpdateScript {
	void run(float ft, fb::Entity* owner) override {
		owner->_relativeRot = glm::rotate(owner->_relativeRot, 0.004f, glm::vec3(0, 1, 0));
	}
};

float totalCamYaw = 0;
float totalCamPitch = 0;
float maxCamPitch =80;
float maxNegCamPitch = -180;

int mouseX = 0, mouseY = 0;

class GameCamUpdater : public fb::UpdateScript {
	void run(float ft, fb::Entity* owner) override {
		if (g_useDebugCam || g_debugState == Debugging) return;

		
		SDL_GetRelativeMouseState(&mouseX, &mouseY);
			
			totalCamYaw += mouseX;
			// Move mouse up
			if (mouseY < 0) {
				if (totalCamPitch > (maxNegCamPitch + mouseY))
					totalCamPitch += mouseY;
			}
			else {
				if (totalCamPitch < (maxCamPitch + mouseY))
					totalCamPitch += mouseY;
			}
			

			owner->_relativeRot = glm::rotate(MATID, -g_camRotSpeed * totalCamYaw, glm::vec3(0, 1, 0));
			owner->_relativeRot = glm::rotate(owner->_relativeRot, -g_camRotSpeed * totalCamPitch, glm::vec3(1, 0, 0));


		glm::vec3 camRight = glm::column(owner->getAbsoluteWorldRot(), 0);
		glm::vec3 camUp = glm::vec3(0, 1, 0);
		glm::vec3 moveDirFlat = glm::cross(camUp, camRight);

		if (g_kbState[SDL_SCANCODE_W]) {
			
			owner->_relativePos += moveDirFlat * g_camSpeed;
		}

		if (g_kbState[SDL_SCANCODE_S]) {
			owner->_relativePos -= moveDirFlat * g_camSpeed;
		}

		if (g_kbState[SDL_SCANCODE_D]) {
			// strafe = move along the right axis
			owner->_relativePos += camRight * g_camSpeed;
		
		}

		if (g_kbState[SDL_SCANCODE_A]) {
			// strafe = move along the right axis
			owner->_relativePos -= camRight * g_camSpeed;
		
		}

	}
};

class GunUpdater : public fb::UpdateScript {
	void run(float ft, fb::Entity* owner) override {
		//owner->_relativePos =  g_GameCameraEntity->_relativePos + glm::vec3(0.5, -0.8, 0);

		//glm::vec3 camRight = glm::column(g_GameCameraEntity->getAbsoluteWorldRot(), 0);
		//glm::vec3 camUp = glm::column(g_GameCameraEntity->getAbsoluteWorldRot(), 1);
		//glm::vec3 camFw = glm::column(g_GameCameraEntity->getAbsoluteWorldRot(), 2);

		//owner->_relativeRot = glm::column(owner->_relativeRot, 0, glm::vec4(camRight, 0));
		//owner->_relativeRot = glm::column(owner->_relativeRot, 1, glm::vec4(camUp, 0));
		//owner->_relativeRot = glm::column(owner->_relativeRot, 2, glm::vec4(camFw, 0));
		
	}
};

class ProjectileUpdater : public fb::UpdateScript {
public:
	
	void run(float ft, fb::Entity* owner) override {
		owner->_relativePos =  owner->_relativePos + glm::vec3(glm::normalize(owner->_relativeRot[2])) * -g_camSpeed;

	}

	
};

void setupEntities() {
	g_TankBody = new fb::Entity(glm::vec3(-10, -0.6, 3), MATID, "TankBody");
	g_TankTurret = new fb::Entity(glm::vec3(0, 1, 0), glm::rotate(MATID, 45.0f, glm::vec3(0, 1, 0)), "TankTurret");
	g_TankBody->addChild(g_TankTurret);
	glm::vec3 turretWorldPos = g_TankTurret->getAbsoluteWorldPos();
	AddLog("turretWorldPos: %f %f %f\n", turretWorldPos.x, turretWorldPos.y, turretWorldPos.z);

	tankBodyModel = importModelFromFile("D:/Projects/C++/opengl_experiments/Debug/tank_body.obj");
	tankTurretModel = importModelFromFile("D:/Projects/C++/opengl_experiments/Debug/tank_turret.obj");
	coordinateSystemModel = importModelFromFile("D:/Projects/C++/opengl_experiments/Debug/coordinate_system.obj");
	projectileModel = importModelFromFile("D:/Projects/C++/opengl_experiments/Debug/projectile.obj");
	
	fb::Model* gunModel = importModelFromFile("D:/Projects/C++/opengl_experiments/Debug/gun.obj");
	fb::Texture* turretTex = new fb::Texture("D:/Projects/C++/opengl_experiments/Debug/turret_tex.bmp", false);
	fb::Texture* axisAll = new fb::Texture("D:/Projects/C++/opengl_experiments/Debug/axis_all.bmp", false);

	g_TankBody->_model = tankBodyModel;
	g_TankTurret->_model = tankTurretModel;
	g_TankBody->_shaderName = "defaultShader";
	g_TankTurret->_shaderName = "defaultShader";
	g_TankBody->_texture = turretTex;
	g_TankTurret->_texture = turretTex;
	gunEntity = new fb::Entity(glm::vec3(0.3, -0.2, 0.5), MATID, "FPSGun");
	gunEntity->_model = gunModel;
	gunEntity->_shaderName = "defaultShader";
	gunEntity->_texture = turretTex;
	gunEntity->addUpdateScript(new GunUpdater());

	g_TankBody->addUpdateScript(new BodyUpdater());
	g_TankTurret->addUpdateScript(new TurretUpdater());
	g_GameCameraEntity->addUpdateScript(new GameCamUpdater());

	g_sceneLevel1 = new fb::Scene();
	fb::Entity* tank = new fb::Entity(glm::vec3(0, 0, 0), MATID, "Tank01");
	tank->addChild(g_TankBody);
	
	g_sceneLevel1->addEntity(tank);
	g_sceneLevel1->addEntity(g_DebugCameraEntity);
	g_sceneLevel1->addEntity(g_GameCameraEntity);
	//g_sceneLevel1->addEntity(gunEntity);

	
	
}

void setActiveCamera(fb::CameraEntity* cam) {
	g_ActiveCameraEntity = cam;
	g_ActiveCamera = cam->_camera;
	if (g_sceneLevel1)
		g_sceneLevel1->setActiveCamera(cam);
}

void renderEntityTree(fb::Entity* e) {
	if (ImGui::TreeNode(e->_name.c_str())) {

		ImGui::Indent();
		ImGui::Text("Pos: ");
		ImGui::SameLine();
		ImGui::Text("%f/%f/%f", e->getAbsoluteWorldPos().x, e->getAbsoluteWorldPos().y, e->getAbsoluteWorldPos().z);
		ImGui::Text("Rot: ");
		ImGui::SameLine();
		ImGui::Text("%f/%f/%f", glm::column(e->getAbsoluteWorldRot(), 0).x, glm::column(e->getAbsoluteWorldRot(), 0).y, glm::column(e->getAbsoluteWorldRot(), 0).z);
		ImGui::Indent(); ImGui::Indent();
		ImGui::Text("%f/%f/%f", glm::column(e->getAbsoluteWorldRot(), 1).x, glm::column(e->getAbsoluteWorldRot(), 1).y, glm::column(e->getAbsoluteWorldRot(), 1).z);
		ImGui::Text("%f/%f/%f", glm::column(e->getAbsoluteWorldRot(), 2).x, glm::column(e->getAbsoluteWorldRot(), 2).y, glm::column(e->getAbsoluteWorldRot(), 2).z);
		ImGui::Text("%f/%f/%f", glm::column(e->getAbsoluteWorldRot(), 3).x, glm::column(e->getAbsoluteWorldRot(), 3).y, glm::column(e->getAbsoluteWorldRot(), 3).z);
		ImGui::Unindent(); ImGui::Unindent();
		ImGui::Unindent();
		for (const auto& c : e->_children) {
			renderEntityTree(c);
		}

		ImGui::TreePop();
	}

}

void spawnProjectile() {
	fb::Entity* projectile = new fb::Entity(g_GameCameraEntity->getAbsoluteWorldPos() + g_GameCameraEntity->_camera->_forward, g_GameCameraEntity->getAbsoluteWorldRot(), "projectile");
	fb::Texture* turretTex = new fb::Texture("D:/Projects/C++/opengl_experiments/Debug/turret_tex.bmp", false);

	projectile->_model = projectileModel;
	projectile->_texture = turretTex;
	projectile->_shaderName = "defaultShader";
	projectile->addUpdateScript(new ProjectileUpdater());
	g_sceneLevel1->addEntity(projectile);
}


int main(int argc, char** args) {
	if (SDL_Init(SDL_INIT_VIDEO) < 0) {
		SDL_Log("error init %s", SDL_GetError());
		AddLog("error init SDL %s", SDL_GetError());
		return -1;
	}

	AddLog("SDL Init ok!\n");

	SDL_GL_SetAttribute(SDL_GL_CONTEXT_MAJOR_VERSION, 4);
	SDL_GL_SetAttribute(SDL_GL_CONTEXT_MINOR_VERSION, 3);
	SDL_GL_SetAttribute(SDL_GL_CONTEXT_PROFILE_MASK, SDL_GL_CONTEXT_PROFILE_CORE);

	SDL_Window* window = SDL_CreateWindow("RoboHunt",
		100, 100,
		g_ScreenWidth, g_ScreenHeight,
		SDL_WINDOW_OPENGL);



	SDL_GLContext glContext = SDL_GL_CreateContext(window);

	const GLubyte* version = glGetString(GL_VERSION);
	AddLog("GL Version: %s\n", version);
	version = glGetString(GL_SHADING_LANGUAGE_VERSION);
	AddLog("Shader version: %s\n", version);

	bool gameRunning = true;



	SDL_Event event;
	glClearColor(0, 0, 0, 1);

	glewExperimental = GL_TRUE;
	GLenum error = glewInit();
	if (GLEW_OK != error) {
		AddLog("glew init failed %s\n", glewGetErrorString(error));
		return -1;
	}

	GLint majVer, minVer;
	glGetIntegerv(GL_MAJOR_VERSION, &majVer);
	glGetIntegerv(GL_MINOR_VERSION, &minVer);
	AddLog("GL Version %d/%d\n", majVer, minVer);

	// Init IMGUI
	IMGUI_CHECKVERSION();
	ImGui::CreateContext();
	ImGuiIO& io = ImGui::GetIO(); (void)io;
	ImGui_ImplSDL2_InitForOpenGL(window, glContext);
	const char* glsl_version = "#version 430";
	ImGui_ImplOpenGL3_Init(glsl_version);
	ImGui::StyleColorsDark();

	// Init GL stuff
	fb::initStaticGeometry();
	glViewport(0, 0, g_ScreenWidth, g_ScreenHeight);
	
	

	fb::ShaderProgram shader = fb::getShaderManager().createShader("D:/Projects/C++/opengl_experiments/opengl_experiments/shaders/vs.hlsl", "D:/Projects/C++/opengl_experiments/opengl_experiments/shaders/fs.hlsl", "defaultShader");
	error = glGetError();
	AddLog("error after creating shader prog: %d\n", error);
	AddLog("prog handle: %u\n", shader);

	glEnable(GL_DEPTH_TEST);
	glEnable(GL_CULL_FACE);
	glFrontFace(GL_CCW);
	glEnable(GL_BLEND);
	glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
	
	fb::Model model2 = *importModelFromFile("D:/Projects/C++/opengl_experiments/Debug/box.obj");
	fb::Model model3 = *importModelFromFile("D:/Projects/C++/opengl_experiments/Debug/hunter_ship.obj");
	fb::Model ground = *importModelFromFile("D:/Projects/C++/opengl_experiments/Debug/ground.obj", glm::vec2(4, 4));
	fb::Model wall = *importModelFromFile("D:/Projects/C++/opengl_experiments/Debug/wall.obj");
	fb::Model gun = *importModelFromFile("D:/Projects/C++/opengl_experiments/Debug/gun.obj");
	fb::Model axis = *importModelFromFile("D:/Projects/C++/opengl_experiments/Debug/axis.obj");
	fb::Model coordinateSystem = *importModelFromFile("D:/Projects/C++/opengl_experiments/Debug/coordinate_system.obj");
	

	GLenum err = glGetError();
	AddLog("glerror: %d\n", err);

	vectorTests();

	fb::Texture texBox("D:/Projects/C++/opengl_experiments/Debug/boxtex.bmp", false);
	fb::Texture texShip("D:/Projects/C++/opengl_experiments/Debug/shiptex.bmp", false);
	
	glm::mat4 mwv = glm::mat4(1.0f);
	static float angle = 0.0f;
	
	glm::mat4 scalem = glm::scale(glm::mat4(1.0f), glm::vec3(2, 2, 2));
	glm::mat4 translatem = glm::translate(glm::mat4(1.0f), glm::vec3(-0, 0, 0));
	
	setupCameras();
	setActiveCamera(g_GameCameraEntity);
	

	fb::ShaderProgram shader2d = fb::getShaderManager().createShader("D:/Projects/C++/opengl_experiments/opengl_experiments/shaders/vs_2d.hlsl", "D:/Projects/C++/opengl_experiments/opengl_experiments/shaders/fs_2d.hlsl", "spriteShader");
	fb::getShaderManager().createShader("D:/Projects/C++/opengl_experiments/opengl_experiments/shaders/vs_2d.hlsl", "D:/Projects/C++/opengl_experiments/opengl_experiments/shaders/fs_2d_depth.hlsl", "depthShader");

	fb::Texture fontTexture("D:/Projects/C++/opengl_experiments/Debug/font.bmp", false);
	fb::Texture marsTexture("D:/Projects/C++/opengl_experiments/Debug/mars-texture.bmp", true);
	fb::Texture crossHairTex("D:/Projects/C++/opengl_experiments/Debug/crosshair.bmp", false);
	fb::Texture axisBlueTex("D:/Projects/C++/opengl_experiments/Debug/axis_blue.bmp", false);
	fb::Texture axisRedTex("D:/Projects/C++/opengl_experiments/Debug/axis_red.bmp", false);
	fb::Texture axisGreenTex("D:/Projects/C++/opengl_experiments/Debug/axis_green.bmp", false);
	fb::Texture axisAll("D:/Projects/C++/opengl_experiments/Debug/axis_all.bmp", false);
	fb::Texture turretTex("D:/Projects/C++/opengl_experiments/Debug/turret_tex.bmp", false);
	fb::Sprite crossHairSprite(crossHairTex, g_ScreenWidth, g_ScreenHeight);

	// End orthographic stuff

	// Sprite setup 
	fb::Texture xyzTex("D:/Projects/C++/opengl_experiments/Debug/xyz_tex.bmp", false);
	fb::Texture titleTex("D:/Projects/C++/opengl_experiments/Debug/robohunt_titlescreen_800x600.bmp", false);
	fb::Sprite sprite1(xyzTex, g_ScreenWidth, g_ScreenHeight);
	fb::Sprite fontSprite(fontTexture, g_ScreenWidth, g_ScreenHeight);
	fb::Sprite titleSprite(titleTex, g_ScreenWidth, g_ScreenHeight);
	fontSprite.scale(0.25f, 0.25f);
	// end sprite setup

	// Framebuffer setup
	fb::FrameBuffer fb1(g_ScreenWidth, g_ScreenHeight);

	// setup sprite to debug-render the shadow map
	fb::Sprite offScreenSprite(fb1._depthTexture, g_ScreenWidth, g_ScreenHeight);
	
	glm::vec3 lightPos(2, 30, 0);

	g_kbState = SDL_GetKeyboardState(NULL);
	SDL_SetRelativeMouseMode(SDL_TRUE);

	glm::mat4 gunRot = glm::rotate(glm::mat4(1), -g_camRotSpeed, g_ActiveCamera->_up);
	glm::vec3 gf = glm::vec3(0, 0, 1);
	glm::vec3 gr = glm::vec3(1, 0, 0);
	glm::vec3 gu = glm::vec3(0, 1, 0);
	float gunRotY = 0.0f;

	gunRot = glm::column(gunRot, 0, glm::vec4(gr, 0));
	gunRot = glm::column(gunRot, 1, glm::vec4(gu, 0));
	gunRot = glm::column(gunRot, 2, glm::vec4(gf, 0));

	setupEntities();

	setupSkeletalMeshes();

	initFmodSound();



	while (gameRunning) {
		while (SDL_PollEvent(&event)) {

			ImGui_ImplSDL2_ProcessEvent(&event);

			if (event.type == SDL_QUIT) {
				gameRunning = false;
				break;
			}

			if (event.type == SDL_MOUSEBUTTONDOWN) {
				if (event.button.button == SDL_BUTTON_LEFT) {
					FMOD_RESULT result = g_soundSystem->playSound(sound1, 0, false, &channel);
					if (result != FMOD_OK) {
						SDL_Log("Sound error: %d", result);
					}

					spawnProjectile();
					
					

				}
			}

			if (event.type == SDL_KEYUP) {
				if (event.key.keysym.scancode == SDL_SCANCODE_O) {
					SDL_Keymod keyMod = SDL_GetModState();
					if (keyMod & KMOD_CTRL) {
						AddLog("CTRL-D Pressed -> toggle DebugMode\n");
						if (g_debugState == DebugState::NonDebug) {
							g_debugState = DebugState::Debugging;
							SDL_SetRelativeMouseMode(SDL_FALSE);
							showDebugWindow = true;
						}
						else {
							g_debugState = DebugState::NonDebug;
							SDL_SetRelativeMouseMode(SDL_TRUE);
							showDebugWindow = false;
						}
					}
				}
				
			}
		}

		// Update the sound system
		FMOD_RESULT result = g_soundSystem->update();
		if (result != FMOD_OK) SDL_Log("error updating the sound system: %d", result);
		
		// Imgui rendering
		ImGui_ImplOpenGL3_NewFrame();
		ImGui_ImplSDL2_NewFrame(window);
		ImGui::NewFrame();

		
		if (showDebugWindow) {
			ImGui::Begin("Debug Console", &showDebugWindow);   // Pass a pointer to our bool variable (the window will have a closing button that will clear the bool when clicked)
			ImGui::Checkbox("Debug Cam", &g_useDebugCam);
			ImGui::SameLine();
			ImGui::Checkbox("Pause Game", &g_pauseGame);
			
			ImGui::Separator();

			ImGui::Text("camPitch: %f", totalCamPitch);
			ImGui::Text("mouseY: %d", mouseY);
			
			ImGui::InputFloat("CamSpeed:", &g_camSpeed, 0.01, 1.0);
			

			ImGui::Separator();

			if (ImGui::CollapsingHeader("Scenegraph")) {
				for (const auto& e : g_sceneLevel1->_entities) {
					renderEntityTree(e);
				}
			}
			
			if (ImGui::CollapsingHeader("Log")) {
				ImGui::TextUnformatted(logBuf.begin());
				if (scrollLogToBottom)
					ImGui::SetScrollHere(1.0f);
				scrollLogToBottom = false;
			}
			
			ImGui::End();
		}
		
		ImGui::Render();

		// end imgui rendering

		if (g_useDebugCam) {
			setActiveCamera(g_DebugCameraEntity);
		}
		else {
			setActiveCamera(g_GameCameraEntity);
		}
	
		glViewport(0, 0, g_ScreenWidth, g_ScreenHeight);
		glClearColor(0, 0, 0, 1);
		glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

		
		if (g_kbState[SDL_SCANCODE_ESCAPE]) {
			gameRunning = false;
		}

		if (g_useDebugCam) {
			debugCamMovement();
		}
		
		angle += 0.01f;
		glm::mat4 rm = glm::rotate(glm::mat4(1.0f), angle, glm::vec3(0, 1, 0));

		// Shadow rendering
		// Render all shadow casters from the lights perspective into an offscreen buffer
		// (= our "shadow map")
		fb1.bind();
		glClearColor(0, 0, 1, 1);
		glViewport(0, 0, g_ScreenWidth, g_ScreenHeight);
		GLfloat cleardepth[] = { 1 };
		glClearBufferfv(GL_DEPTH, 0, cleardepth);
		
		fb::Camera lightCam(lightPos, glm::vec3(0, 0, 0));
		lightCam.initializeOrthographic(-70, 70, -70, 70);
		
		model2.render(glm::translate(glm::mat4(1.0f), glm::vec3(5, 1.5, -2)), lightCam.view, lightCam.proj, glm::mat4(1), scalem, shader, texBox, lightPos, nullptr);
		model2.render(glm::translate(glm::mat4(1.0f), glm::vec3(-5, 1.5, 0)), lightCam.view, lightCam.proj, glm::mat4(1), scalem, shader, texBox, lightPos, nullptr);
		model3.render(glm::translate(glm::mat4(1.0f), glm::vec3(5, 15, -5)), lightCam.view, lightCam.proj, rm, scalem, shader, texShip, lightPos, nullptr);
		model3.render(glm::translate(glm::mat4(1.0f), glm::vec3(10, 2, 5)), lightCam.view, lightCam.proj, MATID, MATID, shader, texShip, lightPos, nullptr);
		wall.render(glm::translate(glm::mat4(1.0f), glm::vec3(25, -0.6, 0)), lightCam.view, lightCam.proj, glm::rotate(glm::mat4(1), 1.57f, glm::vec3(0, 1, 0)), glm::scale(MATID, glm::vec3(2, 5, 1)), shader, texShip, lightPos, nullptr);
		
		fb1.unbind();
		// end shadow map rendering

		// Render our 3D scene
		glViewport(0, 0, g_ScreenWidth, g_ScreenHeight);
		// individually rendered models - temporary for testing purposes
		model2.render(glm::translate(glm::mat4(1.0f), glm::vec3(5, 1.5, -2)), g_ActiveCamera->view, g_ActiveCamera->proj, MATID, scalem, shader, texBox, lightPos, &offScreenSprite._tex);
		model2.render(glm::translate(glm::mat4(1.0f), glm::vec3(-5, 1.5, 0)), g_ActiveCamera->view, g_ActiveCamera->proj, MATID, scalem, shader, texBox, lightPos, nullptr);
		model3.render(glm::translate(glm::mat4(1.0f), glm::vec3(5, 15, -5)), g_ActiveCamera->view, g_ActiveCamera->proj, rm, scalem, shader, texShip, lightPos, nullptr);
		model3.render(glm::translate(glm::mat4(1.0f), glm::vec3(10, 2, 5)), g_ActiveCamera->view, g_ActiveCamera->proj, MATID, MATID, shader, texShip, lightPos, nullptr);
		
		ground.render(glm::translate(glm::mat4(1.0f), glm::vec3(-0, -1, 0)), g_ActiveCamera->view, g_ActiveCamera->proj, MATID, glm::scale(glm::mat4(1.0f), glm::vec3(1, 1, 1)), shader, marsTexture, lightPos, &offScreenSprite._tex);
		ground.render(glm::translate(glm::mat4(1.0f), glm::vec3(-0, -1, -40)), g_ActiveCamera->view, g_ActiveCamera->proj, MATID, glm::scale(glm::mat4(1.0f), glm::vec3(1, 1, 1)), shader, marsTexture, lightPos, &offScreenSprite._tex);
		ground.render(glm::translate(glm::mat4(1.0f), glm::vec3(-40, -1, 0)), g_ActiveCamera->view, g_ActiveCamera->proj, MATID, glm::scale(glm::mat4(1.0f), glm::vec3(1, 1, 1)), shader, marsTexture, lightPos, &offScreenSprite._tex);
		ground.render(glm::translate(glm::mat4(1.0f), glm::vec3(40, -1, 0)), g_ActiveCamera->view, g_ActiveCamera->proj, MATID, glm::scale(glm::mat4(1.0f), glm::vec3(1, 1, 1)), shader, marsTexture, lightPos, &offScreenSprite._tex);
		ground.render(glm::translate(glm::mat4(1.0f), glm::vec3(40, -1, -40)), g_ActiveCamera->view, g_ActiveCamera->proj, MATID, glm::scale(glm::mat4(1.0f), glm::vec3(1, 1, 1)), shader, marsTexture, lightPos, &offScreenSprite._tex);
		ground.render(glm::translate(glm::mat4(1.0f), glm::vec3(-40, -1, -40)), g_ActiveCamera->view, g_ActiveCamera->proj, MATID, glm::scale(glm::mat4(1.0f), glm::vec3(1, 1, 1)), shader, marsTexture, lightPos, &offScreenSprite._tex);
		ground.render(glm::translate(glm::mat4(1.0f), glm::vec3(-0, -1, 40)), g_ActiveCamera->view, g_ActiveCamera->proj, MATID, glm::scale(glm::mat4(1.0f), glm::vec3(1, 1, 1)), shader, marsTexture, lightPos, &offScreenSprite._tex);
		ground.render(glm::translate(glm::mat4(1.0f), glm::vec3(-40, -1, 40)), g_ActiveCamera->view, g_ActiveCamera->proj, MATID, glm::scale(glm::mat4(1.0f), glm::vec3(1, 1, 1)), shader, marsTexture, lightPos, &offScreenSprite._tex);
		ground.render(glm::translate(glm::mat4(1.0f), glm::vec3(40, -1, 40)), g_ActiveCamera->view, g_ActiveCamera->proj, MATID, glm::scale(glm::mat4(1.0f), glm::vec3(1, 1, 1)), shader, marsTexture, lightPos, &offScreenSprite._tex);

		wall.render(glm::translate(glm::mat4(1.0f), glm::vec3(25, -0.6, 0)), g_ActiveCamera->view, g_ActiveCamera->proj, glm::rotate(MATID, 1.57f, glm::vec3(0, 1, 0)), glm::scale(MATID, glm::vec3(2, 5, 1)), shader, texShip, lightPos, nullptr);

		// Render our scene
		if (!g_pauseGame) {
			g_sceneLevel1->newFrame();
			g_sceneLevel1->update(0.01);
		}
		g_sceneLevel1->render();
		g_sceneLevel1->endFrame();

		// Render the gun separately
		if (!g_useDebugCam) {
			fb::Camera dummyGunCam(glm::vec3(-0.4, 0.8, 1.3), glm::vec3(0, 0, -1), glm::vec3(1, 0, 0), glm::vec3(0, 1, 0));
			dummyGunCam.initializePerspective(70);
			gunEntity->renderAsShadowReceiver(&dummyGunCam, lightPos, &offScreenSprite._tex);
		}

		cubicSM->render(g_GameCameraEntity->_camera, lightPos, nullptr, &turretTex);
		
		// UI stuff
		//sprite1.render(450, 200, -30);
		//fontSprite.render(300, 300, -40);
		ImGui_ImplOpenGL3_RenderDrawData(ImGui::GetDrawData());
		
		//offScreenSprite.scale(0.5, 0.5);
		//offScreenSprite.renderFlippedDepth(200, 100, -20);
		crossHairSprite.render(g_ScreenWidth/2, g_ScreenHeight/2, -20);
		
		err = glGetError();
#if true
		if (err != 0)
			AddLog("glError in loop: %d", err);
#endif
		SDL_GL_SwapWindow(window);
	}


	return 0;

}
