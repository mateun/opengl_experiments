#pragma once
#include <vector>
#include <glm/glm.hpp>
#include <string>
#include <map>
#include "camera.h"
#include "model.h"

class aiNode;



namespace fb {

	struct KeyFrame {
		float time;
		glm::mat4 rot;
		glm::vec3 pos;
	};

	struct BoneWeightInfo {
		std::string boneName;
		float weight;
		uint32_t vertexId;
	};

	struct BoneInfo {
		std::string boneName;
		glm::mat4 parentTransform;
	};

	struct AnimationInfo {
		std::string name;
		std::map<std::string, std::vector<KeyFrame>> boneKeyFrames;
	};

	class SkeletalMesh {
	public:
		SkeletalMesh();
		SkeletalMesh(Model* model, std::vector<glm::vec3> boneIds, std::vector<glm::vec3> boneWeights, std::vector<glm::mat4> boneTransforms);
		void initializeFromFile(const std::string& fileName, glm::vec3 textureScale = glm::vec3(1));
		void render(Camera * camera, glm::vec3 lightPos, Texture * shadowMap, Texture* diffuseTexture);
		void updateAnimation(float dt);
		void startAnimation(const std::string& name);

		Model* _model;
		std::vector<glm::vec3> _boneIds;
		std::vector<glm::vec3> _boneWeights;
		std::vector<glm::mat4> _boneTransforms;
		std::vector<AnimationInfo> _animationInfos;
		std::map<uint32_t, std::vector<BoneWeightInfo>> _boneWeightInfos;
		std::vector<BoneInfo> _boneNodes;
		
	private:
		void collectBoneNodes(aiNode* parent, std::vector<BoneInfo>& nodes);
		std::string generateAnimationName();
		uint32_t _animationIndex = 0;

		AnimationInfo* _currentAnimation;
		float _currentAnimationTime;
	};
}

