#pragma once
#include <vector>
#include <glm/glm.hpp>
#include <glm/gtc/matrix_access.hpp>
#include <string>
#include "model.h"
#include "camera.h"
#include "shaderprogram.h"
#include "macros.h"


namespace fb {

	class Entity;

	class UpdateScript {
	public:
		virtual void run(float frameTime, Entity* owner) = 0;
	};

	class Entity {
		public:
			Entity(glm::vec3 relativePos, glm::mat4 relativeRot, const std::string& name);
			void addChild(Entity* entity);
			virtual void update(float frameTime);
			void render(Camera* camera, glm::vec3 lightPos);
			void renderAsShadowReceiver(Camera* camera, glm::vec3 lightPos, Texture* shadowMap);
			void addUpdateScript(UpdateScript* updateScript) { _updateScripts.push_back(updateScript); }

			// This delivers the concatenated positions of the complete 
			// hierarchy up to "this" entity. 
			// E.g. if we have a 2 element hierarchy, 
			// a parent A and a child B. 
			// Lets say A is at (10, 0, 5)
			// And lets say B was initialized with relative position
			// (2, 1, 1)
			// Then the result of this method when called for B is
			// (12, 1, 6)
			glm::vec3 getAbsoluteWorldPos();
			glm::mat4 getAbsoluteWorldRot();

			Entity* _parent;
			std::vector<Entity*> _children;
			Model* _model;
			Texture* _texture;
			std::string _shaderName;
		
			glm::vec3 _relativePos;
			glm::mat4 _relativeRot;

			bool _shallRender = true;
			std::string _name;

	protected: 
		void concatPositions(Entity* ent, glm::vec3& pos);
		void concatRotations(Entity* ent, glm::mat4& rot);
		std::vector<UpdateScript*> _updateScripts;

	};

	class CameraEntity : public Entity {
	public: 
		CameraEntity(glm::vec3 relativePos, glm::mat4 relativeRot, const std::string& name, Camera* camera, bool isDebugCam);
		void update(float frameTime) override;
		
		bool _isDebugCam = false;
		Camera* _camera;
	};
	
}



