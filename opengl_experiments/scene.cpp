#include "stdafx.h"
#include "scene.h"

void fb::Scene::newFrame()
{
	for (const auto& ne : _addedEntities) {
		_entities.push_back(ne);
	}

	_addedEntities.clear();

	for (const auto& re : _removedEntities) {
		for (const auto& e : _entities) {
			// TODO add...
		}
	}
}

void fb::Scene::endFrame()
{
}

void fb::Scene::update(float ft)
{
	for (const auto& e : _entities) {
		e->update(ft);
	}
}

void fb::Scene::render()
{
	for (const auto& e : _entities) {
		e->render(_activeCamera->_camera, _lightPos);
	}
}
