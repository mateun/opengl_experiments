#pragma once
#include <vector>
#include "entity.h"
#include "camera.h"

namespace fb {
	class Scene {
	public:
		void newFrame();
		void endFrame();
		void update(float ft);
		void render();
		void addEntity(Entity* ent) { _addedEntities.push_back(ent); }
		void setActiveCamera(CameraEntity* cam) { _activeCamera = cam; }
		void setLightPos(glm::vec3 lp) { _lightPos = lp; }

		std::vector<Entity*> _entities;
		std::vector<Entity*> _addedEntities;
		std::vector<Entity*> _removedEntities;
		CameraEntity* _activeCamera;
		glm::vec3 _lightPos;


	};
}
