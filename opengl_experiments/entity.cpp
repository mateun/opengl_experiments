#include "stdafx.h"
#include "entity.h"
#include "macros.h"


extern void AddLog(const char* fmt, ...);

fb::Entity::Entity(glm::vec3 relativePos, glm::mat4 relativeRot, const std::string& name)
{
	_name = name;
	_relativePos = relativePos;
	_relativeRot = relativeRot;
}

void fb::Entity::addChild(Entity * entity)
{
	_children.push_back(entity);
	entity->_parent = this;
}

void fb::Entity::update(float frameTime)
{
	for (const auto& us : _updateScripts) {
		us->run(frameTime, this);	
	}

	for (const auto& c : _children) {
		c->update(frameTime);
	}
}

void fb::Entity::render(fb::Camera* camera, glm::vec3 lightPos)
{
	renderAsShadowReceiver(camera, lightPos, nullptr);
}

void fb::Entity::renderAsShadowReceiver(Camera * camera, glm::vec3 lightPos, Texture * shadowMap)
{
	// This _shallRender flag 
	// guides the rendering of the entity itself
	// AND its children. 
	if (_shallRender) {
		if (_model) {
			_model->render(glm::translate(MATID, this->getAbsoluteWorldPos()), camera->view, camera->proj, this->getAbsoluteWorldRot(), MATID, fb::getShaderManager().getShader(_shaderName), *_texture, lightPos, shadowMap);
		}

		// Regardless if we have a model, 
		// our children might have some, so we render 
		// our children in any case.
		for (const auto& c : _children) {
			c->renderAsShadowReceiver(camera, lightPos, shadowMap);
		}
	}
}

glm::vec3 fb::Entity::getAbsoluteWorldPos()
{
	glm::vec3 pos = _relativePos;
	concatPositions(this, pos);
	return pos;
}

glm::mat4 fb::Entity::getAbsoluteWorldRot()
{
	glm::mat4 rot = _relativeRot;
	concatRotations(this, rot);
	return rot;
}

void fb::Entity::concatPositions(Entity * ent, glm::vec3 & pos)
{
	if (ent->_parent != nullptr) {
		pos += ent->_parent->_relativePos;
		concatPositions(ent->_parent, pos);
	}
}

void fb::Entity::concatRotations(Entity * ent, glm::mat4 & rot)
{
	if (ent->_parent != nullptr) {
		rot *= ent->_parent->_relativeRot;
		concatRotations(ent->_parent, rot);
	}
}

fb::CameraEntity::CameraEntity(glm::vec3 relativePos, glm::mat4 relativeRot, const std::string& name, Camera* camera, bool isDebugCam) : 
	Entity(relativePos, relativeRot, name), _camera(camera), _isDebugCam(isDebugCam)
{

}

void fb::CameraEntity::update(float frameTime)
{

	for (const auto& us : _updateScripts) {
		us->run(frameTime, this);
	}

	for (const auto& c : _children) {
		c->update(frameTime);
	}

	if (!_isDebugCam) {
		_camera->pos = getAbsoluteWorldPos();
		_camera->_right = glm::column(getAbsoluteWorldRot(), 0);
		_camera->_up = glm::column(getAbsoluteWorldRot(), 1);
		_camera->_forward = glm::column(getAbsoluteWorldRot(), 2) * -1.0f;
		_camera->updateFromDirections();
	}
	
}
