#include "stdafx.h"
#include "skeletal_mesh.h"
#include <assimp/Importer.hpp>
#include <assimp/scene.h>
#include <assimp/postprocess.h>
#include <glm/glm.hpp>
#define GLM_ENABLE_EXPERIMENTAL
#include <glm/gtx/quaternion.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtc/matrix_access.hpp>
#include "camera.h"
#include "macros.h"

fb::SkeletalMesh::SkeletalMesh()
{
}

fb::SkeletalMesh::SkeletalMesh(Model * model, std::vector<glm::vec3> boneIds, std::vector<glm::vec3> boneWeights, std::vector<glm::mat4> boneTransforms)
	: _boneIds(boneIds), _boneWeights(_boneWeights), _boneTransforms(boneTransforms)
{
}

void fb::SkeletalMesh::render(fb::Camera * camera, glm::vec3 lightPos, fb::Texture * shadowMap, fb::Texture* diffuseTexture)
{
		if (_model) {
			if (_currentAnimation) {
				// 
			}
			_model->render(MATID, camera->view, camera->proj, MATID, MATID, fb::getShaderManager().getShader("defaultShader"), *diffuseTexture, lightPos, shadowMap);
		}
	
}

void fb::SkeletalMesh::updateAnimation(float dt)
{
	_currentAnimationTime += dt;
}

void fb::SkeletalMesh::startAnimation(const std::string & name)
{
	// TODO
	// For now just pull the first animation... 
	_currentAnimation = &_animationInfos[0];
	_currentAnimationTime = 0;
}


void fb::SkeletalMesh::initializeFromFile(const std::string & fileName, glm::vec3 textureScale)
{
	Assimp::Importer importer;
	const aiScene* scene = importer.ReadFile(fileName, aiProcess_Triangulate /*| aiProcess_FlipWindingOrder | aiProcess_GenUVCoords | aiProcess_FixInfacingNormals | aiMakeLeftHanded*/ );

	if (!scene) {
		SDL_Log("can not find model file!");
		exit(-1);
	}

	aiMesh* mesh = scene->mMeshes[0];

	float* vertices = new float[mesh->mNumVertices * 3];
	float* normals = new float[mesh->mNumVertices * 3];
	float* uvs = new float[mesh->mNumVertices * 2];
	uint32_t* indeces = new uint32_t[mesh->mNumFaces * 3];
	int p = 0;
	int u = 0;
	int i = 0;
	for (int m = 0; m < mesh->mNumVertices; m++) {
		aiVector3D v = mesh->mVertices[m];
		vertices[p] = v.x;
		vertices[p + 1] = v.y;
		vertices[p + 2] = v.z;


		aiVector3D n = mesh->mNormals[m];
		normals[p] = n.x;
		normals[p + 1] = n.y;
		normals[p + 2] = n.z;

		p += 3;


		if (mesh->HasTextureCoords(0)) {
			aiVector3D t = mesh->mTextureCoords[0][m];
			uvs[u] = t.x * textureScale.x;
			uvs[u + 1] = (1 - t.y) * textureScale.y;	// otherwise the texture is wrong direction coming from blender
			u += 2;
		}
	}

	for (unsigned int f = 0; f < mesh->mNumFaces; f++) {
		aiFace face = mesh->mFaces[f];
		int findices = face.mNumIndices;
		indeces[i] = face.mIndices[0];
		indeces[i + 1] = face.mIndices[1];
		indeces[i + 2] = face.mIndices[2];
		i += 3;
	}

	_model = new Model(vertices, mesh->mNumVertices, normals, mesh->mNumVertices, uvs, mesh->mNumVertices);
	

	// Pull out the bone information from the mesh
	if (mesh->HasBones()) {
		for (int i = 0; i < mesh->mNumBones; i++) {
			aiBone* bone = mesh->mBones[i];
			aiString name = bone->mName;
			SDL_Log("bone: %s", bone->mName.C_Str());
			aiMatrix4x4 offsetMatrix = bone->mOffsetMatrix;
			aiVector3D offsetScale;
			aiQuaternion offsetRot;
			aiVector3D offsetPos;
			offsetMatrix.Decompose(offsetScale, offsetRot, offsetPos);
			glm::quat rotQuat(offsetRot.w, offsetRot.x, offsetRot.y, offsetRot.z);
			glm::mat4 rotMat = glm::toMat4(rotQuat);
			int numWeights = bone->mNumWeights;
			for (int w = 0; w < numWeights; w++) {
				uint32_t vertexId = bone->mWeights[w].mVertexId;
				float weight = bone->mWeights[w].mWeight;
				SDL_Log("vertex->weight: %u : %f", vertexId, weight);
				BoneWeightInfo bwi;
				bwi.boneName = name.C_Str();
				bwi.weight = weight;
				bwi.vertexId = vertexId;
				_boneWeightInfos[vertexId].push_back(bwi);
			}
		}
	}

	// Pull out the bone nodes
	if (scene->mRootNode) {
		collectBoneNodes(scene->mRootNode, _boneNodes);
		
	}

	// Pull out the animations
	if (scene->HasAnimations()) {
		for (int a = 0; a < scene->mNumAnimations; a++) {
			aiAnimation* animation = scene->mAnimations[a];
			aiString name = animation->mName;
			SDL_Log("animation name: %s", name.C_Str());
			AnimationInfo animationInfo;
			if (std::string(name.C_Str()) == "") {
				animationInfo.name = generateAnimationName();
			}
			else {
				animationInfo.name = name.C_Str();
			}
			for (int c = 0; c < animation->mNumChannels; c++) {
				aiNodeAnim* nodeAnim = animation->mChannels[c];
				int numPosKeys = nodeAnim->mNumPositionKeys;
				int numRotKeys = nodeAnim->mNumRotationKeys;
				int numScaleKeys = nodeAnim->mNumScalingKeys;

				for (int pk = 0; pk < numPosKeys; pk++) {
					aiVectorKey posKey = nodeAnim->mPositionKeys[pk];
					double time = posKey.mTime;
					aiVector3D pos = posKey.mValue;
				}

				for (int rk = 0; rk < numRotKeys; rk++) {
					aiQuatKey rotKey = nodeAnim->mRotationKeys[rk];
					double time = rotKey.mTime;
					aiQuaternion rot = rotKey.mValue;
					glm::quat rotQuat(rot.w, rot.x, rot.y, rot.z);
					glm::mat4 rotMat = glm::toMat4(rotQuat);
					KeyFrame kf;
					kf.time = time;
					kf.rot = rotMat;
					animationInfo.boneKeyFrames[nodeAnim->mNodeName.C_Str()].push_back(kf);
				}
			}

		}

	}

}

void fb::SkeletalMesh::collectBoneNodes(aiNode* parent, std::vector<BoneInfo>& nodes) {
	for (int i = 0; i < parent->mNumChildren; i++) {
		aiNode* node = parent->mChildren[i];
		SDL_Log("node name: %s", node->mName.C_Str());
		BoneInfo bi;
		bi.boneName = node->mName.C_Str();
		aiVector3D offsetScale;
		aiQuaternion offsetRot;
		aiVector3D offsetPos;
		node->mTransformation.Decompose(offsetScale, offsetRot, offsetPos);
		glm::quat rotQuat(offsetRot.w, offsetRot.x, offsetRot.y, offsetRot.z);
		glm::mat4 rot = glm::toMat4(rotQuat);
		bi.parentTransform = rot * glm::translate(glm::mat4(1), glm::vec3(offsetPos.x, offsetPos.y, offsetPos.z)) * glm::scale(glm::mat4(1), glm::vec3(offsetScale.x, offsetScale.y, offsetScale.z));
		nodes.push_back(bi);
		collectBoneNodes(node, _boneNodes);
	}
}

std::string fb::SkeletalMesh::generateAnimationName()
{
	return "Animation_" + std::to_string(_animationIndex++);
	
}

